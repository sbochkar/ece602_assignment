%%%%%%%%%%%%%%%%%%%%%%%%% ECE 602 Final Project %%%%%%%%%%%%%%%%%%%%%%%%%
% Paper: Minimum-Landing-Error Powered-Descent Guidance for Mars Landing 
%        Using Convex Optimization
% Objective: Reproduce experimental results
% Authors: Adam Gomes, Stanislav Bochkarev, and Ryan MacDonald
% Date: March 30, 2016
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%% System parameters for Mars Landing Problem %%%%%%%%%%%%%%% 
%constant acceleration due to gravity
gravity = [-3.1444 0 0]';
%dry mass of lander
mass_dry = 1505; %kg
%wet mass of lander
mass_wet = 1905; %kg
%constant of proportionality between thrust magnitude and mass consumption
alpha = 4.53e-4; %s/m
%lower bound on thrust magnitude
rho_1 = 4972; %N
%upper bound on thrust magnitude
rho_2 = 13260; %N
%initial position of lander
position_0 = [1500 500 2000]'; %m
%Convex Linear Operators
E = [eye(3) zeros(3,4)];
F = [0 0 0 0 0 0 1];
E_u = [eye(3) zeros(3,1)];
E_v = [zeros(3,3) eye(3) zeros(3,1)];
%vector of zeros with unity at index i
e_1 = [1 0 0]';
e_2 = [0 1 0]';
e_3 = [0 0 1]';
%glide-slope constraint angle
gamma = pi*(4/180); %rad
%matrix component of cone constraint 
S = [e_2';e_3'];
%vector component of cone constraint
c = e_1*gamma/tan(gamma);
%number of discrete time steps
N = 55;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Case 1: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% 
%
%
%
%initial velocity of lander
velocity_0 = [-75 0 100]'; %m/s
%optimal final time
tf_opt = 78.4; %s
%time increment
time_step = tf_opt/N;
%Define the State transition matrices (Reference [3] of paper)
A_c = [zeros(3,3) eye(3) zeros(3,1);zeros(3,7);zeros(1,7)];
B_c = [[zeros(3,3);eye(3);0 0 0] [0 0 0 0 0 0 -alpha]'];

Phi_k = zeros(N*7,7);
for k = 1:N
    %Effect of the natural response at time step k
    Phi_k((k-1)*7+1:k*7,1:7) = A_c^k;
end

Lambda_k = zeros(N*7,4);
Lambda_k(1:7,1:4) = B_c;
for k = 2:N
    %Next time step of gravities effect is dependent on the state 
    %transition matrix and the previous time step 
    Lambda_k((k-1)*7+1:k*7,1:4) = A_c*Lambda_k((k-2)*7+1:(k-1)*7,1:4) + B_c;
end

Psi_k = zeros(N*7,4*(N+1));
for k = 2:(N-1)
    %Next time step of gravities effect is dependent on the state TEST ME
    %PLEASE
    %transition matrix and the previous time step 
    Psi_k((k-1)*7+1:k*7,:) = A_c*Psi_k((k-2)*7+1:(k-1)*7,:) +...
        [zeros(7,(k-2)*4) B_c zeros(7,(N-k+1)*4)];
end

M = zeros(N,N*7);
for k = 1:N
    M(k,(k-1)+1:k*7) = F;
end

U = zeros(N,4*(N+1));
for k = 1:N
    U(k,(k-1)*4+1:k*4) = [0 0 0 1];
end


D = zeros(3*N,7*N);
for k = 1:N
    D((k-1)*3+1:k*3,(k-1)*7+1:k*7) = E;
end
V = [zeros(3,(N-1)*7) Ev];


z0_t = log(m_wet-alpha*rho_2*time_step*(0:N)');
I_N = eye(N);

%set of allowed lander positions
cvx_begin
    variable eta(N*4)
    variable y(N*7)
    minimize( norm(D((end-3):end,(end-7):end)*y,2) )
    for k=0:N
        norm(E_u*[zeros(4,(k)*4) eye(4) zeros(4,(N-k)*4)]*eta,2) <= [0 0 0 1]*[zeros(4,(k)*4) eye(4) zeros(4,(N-k)*4)]*eta;
    end
    for k=1:N
        norm(S*(D((k-1)*3+1:k*3,(k-1)*7+1:k*7)*y - D((end-3):end,(end-7):end)*y),2)-c'*(D((k-1)*3+1:k*3,(k-1)*7+1:k*7)*y - D((end-3):end,(end-7):end)*y) <= 0;
    end
    U*eta <= rho_2*exp(-z0_t)*(1-(M*y-z0_t));
    rho_1*exp(-z0_t)*(1-(M*y-z0_t)+0.5*(M*y-z0_t).^2) <= U*eta;
    I_N(N,:)*M*y >= log(mass_dry);
    [zeros(1:(N-1)*7) 1 0 0 0 0 0 0]*y == 0;
    V*y == 0;
    y == Phi_k*[position_0 velocity_0 log(mass_wet)]' + Lambda_k*[g; 0] + Psi_k*eta;
cvx_end
