\documentclass[12pt]{article}
\usepackage{amsmath}
\usepackage{ amssymb }
\usepackage[ruled,vlined]{algorithm2e}
\usepackage{graphicx}

\title{Assignment 1}
\author{Ryan MacDonald, Adam Gomes, Stanislav Bochkarev}
\date{}
\begin{document}
\maketitle

\section*{Principle part}

\subsection*{Exercise 1}

\renewcommand{\theenumi}{\alph{enumi}}
\begin{enumerate}
\item A \emph{slab} is convex as it is the intersection of two half spaces. A half space is convex, and the intersection of convex spaces is always convex.

\item A \emph{rectangle} is convex as it is also the intersection of two \emph{slabs}. From \emph{a)}, we have that a slab is convex and the intersection of convex spaces is always convex.

\item A \emph{wedge} is convex as it is the intersection of two half spaces. This is similar to \emph{a)} but may have the two planes at different angles to each other. Since half spaces are convex, the intersection of two half spaces is also convex. From the set notation of the wedge, it is clear that the wedge is the product of two half spaces as there are two conditions that are separated by a comma which is an intersection operator.
\end{enumerate}

\subsection*{Exercise 2}

Consider a convex line $y = \lambda x_i + (1-\lambda)x_0$ with conditions $0\leq \lambda \leq 1$, $\lambda \in \mathbb{R}$, $x_0, x_i \in \mathbb{R}^n$, $i\in \{1,\ldots,K\}$. For $\lambda \leq \frac{1}{2}$, all points on the convex line are closer to $x_0$ than they are to $x_i$ under the Euclidean norm. By triangular inequality and Pythagorean theorem, the half space bounded at this mid-point and orthogonal to the line is always closer to $x_0$ than $x_i$. Therefore each of these conditions can be written as a half space.
Using the standard form of an orthogonal space one can write the half space as follows:

\begin{gather*} \label{eq1}
\begin{split}
\bigg(\frac{x_i - x_0}{{||x_i - x_0||}_2}\bigg)^T x & \leq {\bigg|\bigg|x_0 + \frac{1}{2}(x_i - x_0)\bigg|\bigg|}_2 \\
\end{split}
i\in[1..K]
\end{gather*}

Let $A$ be the matrix whose $i^{th}$ row is defined as $\big(\frac{x_i - x_0}{{||x_i - x_0||}_2}\big)^T$. Similarly, let $b$ be the vector whose $i^{th}$ row is defined as ${\big|\big|x_0 + \frac{1}{2}(x_i - x_0)\big|\big|}_2$. Finally, $V$ can be expressed as a polyhedron in the form $V = \{x | Ax\preceq b\}$.\\
\begin{figure}[ht]
	\caption{Demonstration of the property.}
	\centering
	\includegraphics[width=0.5\textwidth]{figure.pdf}
	\label{fig:altitude}
\end{figure}
\subsection*{Exercise 3}
Given $f: \mathbb{R} \to \mathbb{R}$ is a convex function the following inequality must hold by definition: $f(\theta a + (1-\theta ) b) \leq \theta f(a) + (1-\theta )f(b)$ for $0 < \theta < 1$, $\theta \in \mathbb{R}$. Let $x\in (a, b)$:

\begin{gather*} \label{eq2}
\frac{f(x) - f(a)}{x - a} \leq \frac{f(b) - f(a)}{b - a} \leq \frac{f(b) - f(x)}{b - x} \\
\end{gather*}

First, looking solely at the left inequality we can write,

\begin{gather*}
\frac{f(x) - f(a)}{x - a} \leq \frac{f(b) - f(a)}{b - a}\\
\frac{f(\theta a + (1-\theta ) b) - f(a)}{\theta a + (1-\theta ) b} \leq \frac{f(b) - f(a)}{b - a} \\
\frac{f(\theta a + (1-\theta ) b) - f(a)}{(b-a)(1-\theta)} \leq \frac{f(b) - f(a)}{b - a}\\
\frac{f(\theta a + (1-\theta ) b) - f(a)}{1-\theta} \leq f(b) - f(a)\\
f(\theta a + (1-\theta ) b) - f(a) \leq (1-\theta)(f(b) - f(a))\\
f(\theta a + (1-\theta ) b)  \leq (1-\theta)f(b) + \theta f(a)\\
\end{gather*}

Similarly, the right hand inequality can be written as,

\begin{gather*}
\frac{f(b) - f(a)}{b - a} \leq \frac{f(b) - f(x)}{b - x} \\
\frac{f(x) - f(b)}{b-x} \leq \frac{f(a) - f(b)}{b - a}\\
\frac{f(\theta a + (1-\theta ) b) - f(b)}{b - \theta a + (1-\theta ) b} \leq \frac{f(a) - f(b)}{b - a}\\
\frac{f(\theta a + (1-\theta ) b) - f(b)}{\theta (b-a)} \leq \frac{f(a) - f(b)}{b - a}\\
f(\theta a + (1-\theta ) b) - f(b) \leq \theta f(a) - \theta f(b)\\
f(\theta a + (1-\theta ) b) \leq (1-\theta)f(b) + \theta f(a)\\
\end{gather*}

Which is the form required. $\square$\\


\subsection*{Exercise 4}
Given $f: \mathbb{R}^n \to \mathbb{R}$ is a differentiable convex function, it must satisfy the first order approximation condition as seen in Boyd's Convex functions slide 3-7. Let $x,y \in \mathbb{R}^n$. 

\begin{gather*} \label{eq3}
\begin{split}
f(y) & \geq f(x) + \nabla f(x)^T(y-x)\\
f(x) & \geq f(y) + \nabla f(y)^T(x-y)\\
\end{split}
\end{gather*}

Let the inequality of $f(x)$ be substituted into the inequality for $f(y)$: 

\begin{gather*}
\begin{split}
f(y) & \geq f(y) + \nabla f(y)^T(x-y) + \nabla f(x)^T(y-x)\\
0 & \geq \nabla f(y)^T(x-y) + \nabla f(x)^T(y-x)\\
0 & \leq \nabla f(x)^T(x-y) - \nabla f(y)^T(x-y)\\
0 & \leq (\nabla f(x)^T - \nabla f(y)^T)(x-y)\\
\end{split}
\end{gather*}

The last line proves the form required. $\square$\\

\section*{Bonus part}

\subsection*{Exercise 1}
Consider the set $C$. The follow properties of $C$ are given:

\begin{enumerate}
\item $bd(C) \subseteq C$
\item $int(C) \neq \emptyset$
\item $a^T x \leq a^T x_0$ given $x_0\in bd(C)$ and $x\in C$
\end{enumerate}

Property 1 shows that property 3 has at least one supporting hyperplane. Property 2 shows that the space within the boundaries is included in $C$.\\ 

\emph{Claim 1:} The supporting hyperplane at $x_0\in bd(C)$ forms a half space which contains all points in $C$.

\emph{Proof:} By property 1 $x_0$ exists. By property 2 interior is not empty. Select any $x_0$ and find its respective $a^T$. Note that $a^T x$ is scalar and the hyperplane defined by every $x\in C$ and $a^T$ must be upper bounded by $a^T x_0$. Therefore all points are contained in the halfspace. $\square$ 

Given every boundary point in $C$ bounds all points with a half space, one can define the set as the interction of all the boundary point's supporting hyperplanes which were proven to form half spaces. Since a half space is convex and the intercetion of convex spaces is always convex, the set $C$ must be convex. $\square$  

Another view: $a^T(x-x_0)\leq 0$ forms a half space. $x - x_0$ simply shifts the entire space to have $x_0$ at the origin. A shift of the entire space does not change its convexity (independent of convex or not). Therefore it proves that each boundary point forms a halfspace and that the intercetion of these halfspaces is not empty (all on the same side of 0). Therefore the intercetion of these halfspaces is exactly $C$ and proves that $C$ is convex. $\square$

\subsection*{Exercise 2}
Consider $X \in \mathbb{S}^n$ with eigenvalues $\lambda_1(X) \leq \lambda_2(X)\ldots \leq \lambda_n(X)$. Show that $\sum_{i=1}^{k}\lambda_i(X)$ is convex on $\mathbb{S}^n$ for $k\leq n$.\\ 

Note that given $k=n$ the sum becomes the trace of $X$. Define the function $tr_k: \mathbb{S}^n \to \mathbb{R}$ which sums the $k$ largest eigenvalues of its input. Using single value decomposition of $X$ one can see $X = A \Lambda A^T$ with the matrix $\Lambda=diag(\lambda_1(X),...,\lambda_n(X))$. One can break $\Lambda$ into two matrixies padded appropriately: $\Lambda=\Lambda_k + \Lambda_{n-k} = diag(\lambda_1(X),...,\lambda_k(X),0,...) + diag(...,0,\lambda_{k+1}(X),...,\lambda_n(X))$. $\therefore$ $X = A\Lambda A^T=A(\Lambda_k + \Lambda_{n-k})A^T = X_k + X_{n-k}$. Consider $Y = aV + (1-a)W$ s.t. $V,W\in \mathbb{S}^n$ which implies $Y\in \mathbb{S}^n$ as $\mathbb{S}^n$ is a vector space. Note the property of trace $tr(A+ B) = tr(A) + tr(B)$.

\begin{gather*}
\begin{split}
tr(Y) & = tr(a(V_k + V_{n-k}) + (1-a)(W_k + W_{n-k}))\\
& = tr((a)V_k) + tr((1-a)W_k)) + tr((a)V_{n-k}) + tr((1-a)W_{n-k})\\
tr(Y_k) + tr(Y_{n-k}) & = tr(aV_k + (1-a)W_k) + tr(aV_{n-k}+ (1-a)W_{n-k})\\
\implies tr(Y_k) & = tr(aV_k) + tr((1-a)W_k)\\
tr(Y_k) & = (a)tr(V_k) + (1-a)tr(W_k)\\
\end{split}
\end{gather*}

By definition of $tr_k$, $tr(Y_k)$ is exacly $tr_k(Y)$. This proves that $tr_k(Y) \leq (a)tr_k(V) + (1-a) tr_k(W)$ as it is always equal. This shows that $tr_k$ is convex on $\mathbb{S}^n$.\\

\subsection*{Exercise 3}
Consider an increasing function $f: \mathbb{R}\to \mathbb{R}$ with $f(0) = 0$, and let $g = f^{-1}$. Define $F(x) =\int_{0}^{x}f(t)dt$ and $G(x) = \int_{0}^{x}g^(t)dt$. 

\emph{Claim:} F and G are conjugates.

\emph{Proof:} Conjugate is defined as the following:

\begin{gather*}
\begin{split}
G(y) & = \sup_{x\in dom(F)} (xy - F(x))\\
\int_{0}^{y}g(t)dt & = \sup_{x\in dom(F)} \bigg(xy - \int_{0}^{x}f(t)dt\bigg)\\
\int_{0}^{y}g(t)dt & = \sup_{x\in dom(F)} \bigg(\int_{0}^{x}(y-f(t))dt\bigg)\\
\int_{0}^{y}g(t)dt & = \sup_{x\in dom(F)} \bigg(\int_{0}^{f^{-1}(y)}(y-f(t))dt+\int_{f^{-1}(y)}^{x}(y-f(t))dt\bigg)\\
\int_{0}^{y}g(t)dt & = \sup_{x\in dom(F)} \bigg(\int_{f^{-1}(y)}^{x}(y-f(t))dt\bigg) + \int_{0}^{f^{-1}(y)}(y-f(t))dt\\
\end{split}
\end{gather*}

Since $f$ is an increasing function any $f^{-1}(y)\leq x \implies y\leq f(x)$ and $y-f(x)$ is always negative. Any $x\leq f^{-1}(y) \implies f(x) \leq y$ and $y-f(x)$ is always positive but calculus tells us that if the upper limit of an integral is smaller than the low simply switch the limits and multiply by -1. Therefore any $x\neq y$ will always be less than 0 which is the maximum possible.    

\begin{gather*}
\begin{split}
\int_{0}^{y}f^{-1}(t)dt & = \int_{0}^{f^{-1}(y)}(y-f(t))dt\\
& = yf^{-1}(y) - \int_{0}^{f^{-1}(y)}(f(t))dt
\end{split}
\end{gather*}

Using a lookup table this is trivially true. (One can do it by parts but running out of time on this assignment.)

\emph{Claim:} Since $f$ is an increasing function with $f(0) = 0$, its inverse is also increasing.

\emph{Proof:} An increasing function, say $h: \mathbb{R}\to \mathbb{R}$, means that  $x\leq y \implies h(x) \leq h(y)$ with $x,y\in\mathbb{R}$. Let its inverse be $p$. There must exist $u,v\in\mathbb{R}$ with $u\leq v$ such that $h(u)=x'\leq h(v)=y'$. Since $p$ is an inverse function: $x'\leq y'\implies p(x')=u \leq p(y')=v$ must also be true. $\square$

Therefore by the claim, $g$ must be an increasing function also. So this also works in reverse. $\square$

\begin{figure}[ht]
	\caption{Young's inequality.}
	\centering
	\includegraphics[width=0.5\textwidth]{conjugate.pdf}
	\label{fig:}
\end{figure}

\end{document}