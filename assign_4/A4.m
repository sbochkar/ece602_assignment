m=256;
t=linspace(0,1,m)';
y=exp(-128*((t-0.3).^2))-3*(abs(t-0.7).^0.4);
%mpdict=wmpdictionary(m,'LstCpt',{'dct',{'wpsym4',2}});
%A=full(mpdict);
load('Amatrix.mat')

N = size(A,2);
one = ones(1,N);
cvx_begin
    variable x(N)
    minimize( norm(x,1) )
    subject to
        A*x == y
cvx_end
x1 = x;
clear x;
N = size(A,2);
one = ones(1,N);
cvx_begin
    variable x(N)
    minimize( norm(x,2)^2 )
    subject to
        A*x == y
cvx_end
x2 = x;

%b
error1 = norm(A*x1-y,2)^2/norm(y,2)^2;
error2 = norm(A*x2-y,2)^2/norm(y,2)^2;
error1
error2

%c
temp1 = zeros(length(x),1);
for i = 1:26
    [val, pos] = max(abs(x1));
    temp1(pos) = x1(pos);
    x1(pos) = 0;
end

temp2 = zeros(length(x),1);
for i = 1:26
    [val, pos] = max(abs(x2));
    temp2(pos) = x2(pos);
    x2(pos) = 0;
end

error1_5 = norm(A*temp1-y,2)^2/norm(y,2)^2;
error2_5 = norm(A*temp2-y,2)^2/norm(y,2)^2;
error1_5
error2_5

figure(1)
hold all;
plot(A*temp1)
plot(A*temp2)
plot(y)
ylabel('A*x')
legend(['l-1 err=', num2str(error1_5)],['l-2 err=', num2str(error2_5)],'y')
title('5% compressed')

%d
x1 = x1+temp1;
temp1 = zeros(length(x),1);
for i = 1:ceil(512*.03)
    [val, pos] = max(abs(x1));
    temp1(pos) = x1(pos);
    x1(pos) = 0;
end

x2 = x2+temp2;
temp2 = zeros(length(x),1);
for i = 1:ceil(512*.03)
    [val, pos] = max(abs(x2));
    temp2(pos) = x2(pos);
    x2(pos) = 0;
end
error1_3 = norm(A*temp1-y,2)^2/norm(y,2)^2;
error2_3 = norm(A*temp2-y,2)^2/norm(y,2)^2;

figure(2)
hold all;
plot(A*temp1)
plot(A*temp2)
plot(y)
ylabel('A*x')
legend(['l-1 err=', num2str(error1_3)],['l-2 err=', num2str(error2_3)],'y')
title('3% compressed')


%finding x2 18% = x1 3%
x2 = x2+temp2;
temp2 = zeros(length(x),1);
for i = 1:ceil(512*.18)
    [val, pos] = max(abs(x2));
    temp2(pos) = x2(pos);
    x2(pos) = 0;
end


x1 = x1+temp1;
temp1 = zeros(length(x),1);
for i = 1:ceil(512*.01)
    [val, pos] = max(abs(x1));
    temp1(pos) = x1(pos);
    x1(pos) = 0;
end

x2 = x2+temp2;
temp2 = zeros(length(x),1);
for i = 1:ceil(512*.01)
    [val, pos] = max(abs(x2));
    temp2(pos) = x2(pos);
    x2(pos) = 0;
end

error1_1 = norm(A*temp1-y,2)^2/norm(y,2)^2;
error2_1 = norm(A*temp2-y,2)^2/norm(y,2)^2;

figure(3)
hold all;
plot(A*temp1)
plot(A*temp2)
plot(y)
ylabel('A*x')
legend(['l-1 err=', num2str(error1_1)],['l-2 err=', num2str(error2_1)],'y')
title('1% compressed')


