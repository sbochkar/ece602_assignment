A = [60 45 -8;90 30 -30;0 -8 -4;30 10 -10];
R = ones(4,3)*.05;
b = [-6 -3 18 -9]';
N = size(A,2);
cvx_begin
    variable x(N)
    minimize( norm(A*x-b,2) )
    subject to
cvx_end
x_ls = x;

cvx_begin
    variable y(4)
    variable z(N)
    variable x(N)
    minimize( y'*y )
    subject to
        A*x+R*z-b <=y
        A*x-R*z-b >=-y
        x<=z
        x>=-z
cvx_end
x_rls = x;

normLS = norm(A*x_ls-b,2)
normRLS = norm(A*x_rls-b,2)

r=A*x_ls-b;
e=zeros(4,3);

for i=1:length(r)
    if r(i)<0
        e(i,:)=-0.05*sign(x_ls');
    else
        e(i,:)=0.05*sign(x_ls');
    end
end

wcnormLS=norm(A*x_ls-b+e*x_ls,2)
wcnormRLS=sqrt(y'*y)