n = 20; % dimension of x's
M = 25; % number of non-censored data points
K = 100; % total number of points
c_true = randn(n,1);
X = randn(n,K);
y = X'*c_true + 0.1*(sqrt(n))*randn(K,1);
% Reorder measurements, then censor
[y, sort_ind] = sort(y);
X = X(:,sort_ind);
D = (y(M)+y(M+1))/2;
y = y(1:M);


cvx_begin
	variables c(n) z(K-M)
	minimize(sum((y'-c'*X(:,1:M)).^2)+sum((z'-c'*X(:,M+1:K)).^2))
	subject to
	z>=D
cvx_end

chat=c;

cvx_begin
	variables c(n)
	minimize(sum((y'-c'*X(:,1:M)).^2))
cvx_end

cls=c;

chatrelerror=norm(c_true-chat)/norm(c_true);
clsrelerrot=norm(c_true-cls)/norm(c_true);

chat
cls
chatrelerror
clsrelerrot