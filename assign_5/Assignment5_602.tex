\documentclass[12pt]{article}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[ruled,vlined]{algorithm2e}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{color}%,soul}
\usepackage[]{algorithm2e}
\usepackage{tikz}
\usepackage{subcaption}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=MATLAB,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

\title{Assignment 5}
\author{Ryan MacDonald, Adam Gomes, Stanislav Bochkarev}
\date{}
\begin{document}
\maketitle

\section*{Principle part}

\subsection*{Exercise 1}
\subsubsection*{(a)}
The Newton step for $f(x)=\log(e^x+e^{-x})$ is
\begin{equation}
\Delta x_{nt}=-\nabla^2f(x)^{-1}\nabla f(x)=\frac{1-e^{4x}}{4e^{2x}}
\end{equation}
For $x^{(0)}=1$, Newton's method converges to the optimal solution $x^*$ in less than 6 steps. Figure \ref{1aa} shows the function $f(x)$, the derivative $f'(x)$, and a few iterations of Newton's method.
\begin{figure}[!htbp]
  \centering
  % Requires \usepackage{graphicx}
  \includegraphics[height=6.5cm]{1aa.png}
  \caption [1aa]
  { %\fontsize{10pt}{0pt}\selectfont
	Newton's method for $x^{(0)}=1$.
  }
  \label{1aa}
\end{figure}
\noindent For $x^{(0)}=1.1$, Newton's method diverges from the optimal solution. Figure \ref{1ab} shows the function $f(x)$, the derivative $f'(x)$, and a few iterations of Newton's method.
\begin{figure}[!htbp]
  \centering
  % Requires \usepackage{graphicx}
  \includegraphics[height=6.5cm]{1ab.png}
  \caption [1ab]
  { %\fontsize{10pt}{0pt}\selectfont
	Newton's method for $x^{(0)}=1.1$.
  }
  \label{1ab}
\end{figure}
\subsubsection*{(b)}
The Newton step for $f(x)=-\log(x)+x$ is
\begin{equation}
\Delta x_{nt}=-\nabla^2f(x)^{-1}\nabla f(x)=-\frac{1}{x^2}+\frac{1}{x^3}
\end{equation}
For $x^{(0)}=3$, Newton's method converges to the optimal solution $x^*$ in approximately 21 steps. Figure \ref{1b} shows the function $f(x)$, the derivative $f'(x)$, and a few iterations of Newton's method.
\begin{figure}[!htbp]
  \centering
  % Requires \usepackage{graphicx}
  \includegraphics[height=6.5cm]{1b.png}
  \caption [1ab]
  { %\fontsize{10pt}{0pt}\selectfont
	Newton's method for $x^{(0)}=3$.
  }
  \label{1b}
\end{figure}

\subsection*{Exercise 2}
The Newton equation is given by $x_{k+1} = x_{k} + t\Delta x$. It requires the newton step that satisfies $\nabla^2 f(x) \Delta x = \nabla f(x)$. Given that the inverse of the Hessian is computational expensive, it would be wise to find an efficient way to compute the step. By referencing the textbook, we can find the Hessian.

\begin{equation*}
\begin{aligned}
&f(x) = \frac{1}{2}x^T x + \log \bigg(\sum_{i=1}^{m} e^{(a_i)^Tx+b_i}\bigg)\\
&\nabla f(x) = x + \frac{1}{\bold{1}^T z}A^T z\\
&\nabla^{2} f(x) = I + A^T\bigg(\frac{1}{\bold{1}^T z}diag(z) -\frac{1}{(\bold{1}^T z)^2}zz^T\bigg)A\\
&\text{where } z_i = e^{(a_i)^Tx+b_i} \text{ are the rows of } z
\end{aligned}
\end{equation*}
 
Therefore using the Woodbury matrix identity (matrix inversion lemma) the system can be reduced.

\begin{equation*}
\begin{aligned}
&C = \bigg(\frac{1}{\bold{1}^T z}diag(z) -\frac{1}{(\bold{1}^T z)^2}zz^T\bigg)\\
&(I + A^TCA)^{-1} = I -A^T(I+CAA^T)^{-1}CA
\end{aligned}
\end{equation*} 

Given $A$ is $\mathbb{R}^{mxn}$ and $m$ is far smaller than $n$, taking the inverse of the Hessian directly would be roughly $n^3$. On the other hand, using the lemma there is a far smaller inversion that can be done i.e.  $(I+CAA^T)^{-1}$. This inversion is on a matrix of size $mxm$. This means we can compute the inverse in roughly $m^3$. Given $m$ is much smaller than $n$ the inversion will be much more efficient. Then the Newton step and Newton equation can be written as follows:

\begin{equation*}
\begin{aligned}
&\Delta x = (I -A^T(I+CAA^T)^{-1}CA)\bigg( x + \frac{1}{\bold{1}^T z}A^T z\bigg)\\
&x_{k+1} = x_{k} + t\Delta x
\end{aligned}
\end{equation*} 


\subsection*{Exercise 3}
Considering the original problem, the newton step satisfies $\nabla^2 f(x) \Delta x = \nabla f(x)$. The Hessian of the modified problem can be written as follows:

\begin{equation*}
\begin{aligned}
&\nabla (f(x) + (Ax-b)^TQ(Ax-b)) = \nabla f(x) + 2A^TQAx -2A^TQb\\
&\nabla^2(f(x) + (Ax-b)^TQ(Ax-b)) = \nabla^2 f(x) + 2A^TQA
\end{aligned}
\end{equation*} 
Solving for the new Newton step:
\begin{equation*}
\begin{bmatrix}
	 \nabla^2 f(x) + 2A^TQA & A ^T\\
	A & 0
\end{bmatrix}
\begin{bmatrix}
	 \Delta x_{new}\\
	w_{new}
\end{bmatrix}
= 
\begin{bmatrix}
	-\nabla f(x) - 2A^TQAx + 2A^TQb\\
	0
\end{bmatrix}
\end{equation*}

$\mathbf{Claim:}$  $\Delta x_{new} =\Delta x$.

Since $A\Delta x_{new} = 0$ by observing row two: 
\begin{equation*}
\begin{bmatrix}
	 \nabla^2 f(x) & A ^T\\
	A & 0
\end{bmatrix}
\begin{bmatrix}
	 \Delta x_{new}\\
	w_{new}
\end{bmatrix}
= 
\begin{bmatrix}
	-\nabla f(x) + 2A^TQ(b-Ax)\\
	0
\end{bmatrix}
\end{equation*}
Since $b-Ax = 0$ by constraints:
\begin{equation*}
\begin{bmatrix}
	 \nabla^2 f(x) & A ^T\\
	A & 0
\end{bmatrix}
\begin{bmatrix}
	 \Delta x_{new}\\
	w_{new}
\end{bmatrix}
= 
\begin{bmatrix}
	-\nabla f(x)\\
	0
\end{bmatrix}\\
\end{equation*}
$w_{new}$ must be the optimal dual variable to the new problem. Looking at KKT:
\begin{equation*}
\begin{aligned}
& \nabla f(x^*) + 2A^TQAx^* -2A^TQb + A^Tw_{new} = 0\\
& \nabla f(x^*) + 2A^TQ(Ax^* -b) + A^Tw_{new} = 0\\
& \nabla f(x^*) + A^Tw_{new} = 0\\
\end{aligned}
\end{equation*} 
The last line is the same as the KKT conditions for the original optimal dual variable $w$. This implies: 
\begin{equation*}
\begin{bmatrix}
	 \nabla^2 f(x) & A ^T\\
	A & 0
\end{bmatrix}
\begin{bmatrix}
	 \Delta x_{new}\\
	w
\end{bmatrix}
= 
\begin{bmatrix}
	-\nabla f(x)\\
	0
\end{bmatrix}\\
\end{equation*}
Finally, this system of equations is identical to that of the original problem $\therefore \Delta x_{new} = \Delta x \>\>\square$
\end{document}

