function x_root=NewtMethodb(x_0)
x=x_0;
x_root=x_0;
for i=1:30
%dx=(1-exp(2*x))*(1+exp(2*x))/(4*exp(2*x));
dx=-1/x^2+1/x^3;
x=x+dx;
x_root=[x_root,x];
end
end