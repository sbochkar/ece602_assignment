close all
x_0=1;
x_root=NewtMethod(x_0);
x=linspace(-5,5,10000);
fx=log(exp(x)+exp(-x));
dfx=(exp(2*x)-1)./(exp(2*x)+1);

fxstar=log(exp(x_root)+exp(-x_root));
fx0=log(exp(x_0)+exp(-x_0));

plot(x,fx,x,dfx);
title('1a Newton Step Method x_0=1');
hold on
plot(x_0,fx0,'*','Color','r');
plot(x_root,fxstar,'Color','r');

legend('f(x)','df/dx','f(x_0)','f(x+\Delta x)')
hold off

x_0=3;
x_root=NewtMethodb(x_0);
x=linspace(0.1,5,5000);
fx=-log(x)+x;
dfx=-1./x+1;

fxstar=-log(x_root)+x_root;
fx0=-log(x_0)+x_0;

figure
plot(x,fx,x,dfx);
title('1b Newton Step Method x_0=3');

hold on
plot(x_0,fx0,'*','Color','r');
plot(x_root,fxstar,'Color','r');
legend('f(x)','df/dx','f(x_0)','f(x+\Delta x)')
